<?php

return [

    /*
    |--------------------------------------------------------------------------
    | English Application Language Lines
    |--------------------------------------------------------------------------
    */
    'buttons' => [
        'share' => 'Share',
        'tweet' => 'Tweet'
    ],
    'footer' => [
        'about'  => 'About Us',
        'latest' => 'Latest Articles',
        'share'  => 'Follow with Social',
        'text'   => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum',
        'url'    => 'https://www.fabiocostanza.it'
    ],
    'pagination' => [
        'next'     => 'Next &raquo;',
        'previous' => '&laquo; Previous',
    ],
    'read_more'    => 'Read More'
];
